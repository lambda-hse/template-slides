# Template slides in HSE LAMBDA style.

**NB** if you get compiler errors --- change your LaTeX compiler to either **XeLaTeX** or **LuaLaTeX**.*

## Official version ([open in overleaf](https://www.overleaf.com/docs?snip_uri=https://gitlab.com/lambda-hse/template-slides/-/archive/master/template-slides-master.zip&engine=xelatex))

Inspired by Yandex style. A courtesy of Vlad Belavin.

## Unofficial version ([open in overleaf](https://www.overleaf.com/docs?snip_uri=https://gitlab.com/lambda-hse/template-slides/-/archive/metropolis/template-slides-metropolis.zip&engine=xelatex))

Metropolis beamer theme with a color change.